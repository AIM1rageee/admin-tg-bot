include .env
export

up:
	docker compose up --detach

down:
	docker compose down

build:
	docker image build -t ${IMAGE_APP} .

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}

migrate:
	python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations

createsuperuser:
	python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

dev:
	python src/manage.py runserver localhost:8000

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

test:
	docker compose run django-app pytest --disable-warnings

prepare_lint:
	apk add py3-isort
	apk add py3-flake8
	apk add black

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

add_comma:
	forfiles /s /m *.py /c "cmd /c add-trailing-comma @path"

release: build up
