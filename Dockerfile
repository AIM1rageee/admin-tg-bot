FROM python:3.10-slim-buster

ENV PYTHONUNBUFFERED=1

COPY Pipfile Pipfile.lock ./
RUN python -m pip install --upgrade pip && \
    pip install pipenv && \
    pipenv install --system --deploy

WORKDIR /
COPY /src /src
COPY Makefile .
COPY .env .
COPY pytest.ini .
RUN mkdir -p /var/log/admin-tg-bot