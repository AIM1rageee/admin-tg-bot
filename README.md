# AdminTgBot

This is a simple project that consists of a server and a bot. It was built with Python and Django framework.

## Usage

To run the server, use the following command:

```
docker compose app
```

To start the bot, use the following command:

```
docker compose bot
```

## Endpoints

- `/api/docs` - Swagger UI documentation for the API.
- `/admin` - Admin page where you can manage all application entities directly.
- `/monitoring/metrics` - This endpoint returns main Sakha-Bank metrics of the application.

## Environment Variables

To set up the environment variables, please refer to the `.env.example` file.

Make a copy of the `.env.example` file and rename it to `.env`. Then, fill in the necessary values.

Please note that the environment variables are essential for the proper functioning of the server and bot, so make sure to provide the required values correctly.

## Important Links

- [Python](https://www.python.org/)
- [Django](https://www.djangoproject.com/)
- [S3Yandex](https://yandex.cloud/ru/docs/storage/s3/)
- [PrometheusYandex](https://yandex.cloud/ru/docs/monitoring/operations/prometheus/)


## Contact

If you have any questions or feedback, you can reach me at @gorosha007 (Telegram).