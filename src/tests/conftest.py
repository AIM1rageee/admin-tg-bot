import psycopg2
import pytest
from django.conf import settings
from django.db import connections
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from app.internal.models.tg_user import TgUser


def run_sql(sql):
    conn = psycopg2.connect(
        database=settings.POSTGRES_DB,
        user=settings.POSTGRES_USER,
        password=settings.POSTGRES_PASSWORD,
        host="pgdb",
        port=5432,
    )
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = conn.cursor()
    cur.execute(sql)
    conn.close()


@pytest.fixture(scope="session", autouse=True)
def django_db_setup():
    settings.DATABASES["default"]["NAME"] = "postgres_copy"

    run_sql("DROP DATABASE IF EXISTS postgres_copy")
    run_sql("CREATE DATABASE postgres_copy TEMPLATE postgres")

    yield

    for connection in connections.all():
        connection.close()

    run_sql("DROP DATABASE postgres_copy WITH (FORCE)")


@pytest.fixture(scope="function")
def registered_users(django_db_setup):
    users = []
    for i in range(10):
        user = TgUser.objects.update_or_create(
            telegram_id=i,
            defaults={
                "username": f"test_user{i}",
                "first_name": "TestUser",
                "phone": "+79999999999",
            },
        )[0]
        user.save()
        users.append(user)
    return users
