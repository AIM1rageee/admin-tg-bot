import json

import pytest
from django.http import JsonResponse


def test_admin_page_code(api_client):
    response = api_client.get("/admin/")
    assert response.status_code == 302


@pytest.mark.django_db
def test_me_endpoint(api_client):
    response: JsonResponse = api_client.get("/api/me/314159")
    user_data = json.loads(response.content)
    assert response.status_code == 200
    assert user_data == {
        "is_registered": False,
    }
