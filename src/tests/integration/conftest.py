import pytest
from django.test import Client


@pytest.fixture(scope="module")
def api_client() -> Client:
    return Client()
