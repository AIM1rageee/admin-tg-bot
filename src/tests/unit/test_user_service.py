from unittest.mock import MagicMock

import pytest

from app.internal.services.user_service import (
    add_favorite,
    create_user,
    get_favorites,
    get_user,
    get_user_by_username,
    get_user_sync,
    remove_favorite,
    set_user_phone,
)


@pytest.mark.django_db
@pytest.mark.asyncio
async def test_create_user():
    message = MagicMock()
    message.chat_id = 1
    message.from_user.username = "test_user"
    message.from_user.first_name = "Test"
    message.from_user.last_name = "User"

    user = await create_user(message)
    assert user is not None
    assert user.telegram_id == message.chat_id
    assert user.username == message.from_user.username
    assert user.first_name == message.from_user.first_name
    assert user.last_name == message.from_user.last_name


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_set_user_phone():
    message = MagicMock()
    message.chat_id = 2
    message.from_user.username = "test_user"
    message.from_user.first_name = "Test"
    message.from_user.last_name = "User"
    message.text = "+79999999999"

    user = await set_user_phone(message)
    assert user.phone == message.text


@pytest.mark.asyncio
async def test_get_user(db, registered_users):
    """
    TODO
    Этот тест никак не проходится (достает None), из-за этого тесты для счета тоже не проходятся,
    потому что тоже достают юзера.

    Вообще не ебу, весь интернет перерыл, причем его sync версия отрабатывает хорошо. Спокойной ночи :)
    """
    telegram_id = 4
    user = await get_user(telegram_id)
    assert user is not None


@pytest.mark.asyncio
async def test_get_user_by_username(db, registered_users):
    username = "test_user1"
    user = await get_user_by_username(username)
    assert user is not None


def test_get_user_sync(db, registered_users):
    telegram_id = 4
    user = get_user_sync(telegram_id)
    assert user is not None


@pytest.mark.asyncio
async def test_get_favorites(db, registered_users):
    telegram_id = 4
    favorites = await get_favorites(telegram_id)
    assert favorites == []


@pytest.mark.asyncio
async def test_add_favorite(db, registered_users):
    user_id = 6
    favorite_username = "test_user1"
    assert await add_favorite(user_id, favorite_username)


@pytest.mark.asyncio
async def test_remove_favorite(db, registered_users):
    user_id = 6
    favorite_id = 1
    assert await remove_favorite(user_id, favorite_id)
