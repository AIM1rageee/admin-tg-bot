import pytest

from app.internal.services.account_service import (
    create_account,
    create_card,
    deposit_random_card,
    get_card,
    get_random_user_card,
    has_card,
    transfer_money_from_card_to_card,
)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_create_account(registered_users):
    telegram_id = 1
    account = await create_account(telegram_id)
    assert account.user_id == telegram_id


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_create_card(registered_users):
    telegram_id = 1
    start_balance = 100.0
    card = await create_card(telegram_id, start_balance)
    assert card.balance == start_balance


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_get_card():
    card_id = 1
    card = await get_card(card_id)
    assert card is not None


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_has_card():
    telegram_id = 3
    assert not await has_card(telegram_id)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_get_random_user_card():
    telegram_id = 1
    random_card = await get_random_user_card(telegram_id)
    assert random_card is None


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_deposit_random_card():
    telegram_id = 1
    amount = 50.0
    card = await deposit_random_card(telegram_id, amount)
    assert card.balance == amount


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_transfer_money_from_card_to_card():
    sender_card_id = 1
    receiver_card_id = 2
    amount = 30.0
    await transfer_money_from_card_to_card(sender_card_id, receiver_card_id, amount)
    sender_card = await get_card(sender_card_id)
    receiver_card = await get_card(receiver_card_id)
    assert sender_card.balance == 70.0
    assert receiver_card.balance == 30.0
