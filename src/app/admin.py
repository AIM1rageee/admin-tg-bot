from django.contrib import admin

from app.internal.bank.presentation.admin import AccountAdmin, CardAdmin, OperationAdmin
from app.internal.tokens.presentation.admin import TokenAdmin
from app.internal.users.presentation.admin import UserAdmin

admin.site.site_title = "Bank"
admin.site.site_header = "Bank"
