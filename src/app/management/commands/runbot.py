from django.core.management.base import BaseCommand

from app.internal.bot import build_application
from config.settings import TG_TOKEN, TG_WEBHOOK_URL


class Command(BaseCommand):
    def handle(self, *args, **options):
        application = build_application()
        application.run_webhook(
            listen="0.0.0.0",
            port=8443,
            url_path="bot",
            secret_token=TG_TOKEN.split(":")[1],
            webhook_url=f"https://{TG_WEBHOOK_URL}/bot",
        )
