from abc import ABC

from telegram import Message

from app.internal.bank.domain.services import BankService
from app.internal.users.domain.services import UserService
from app.template_engine import render_template


class IChecker(ABC):
    def __init__(self, fail_message: str, check_function):
        self.fail_message: str = fail_message
        self.check_function = check_function

    async def check(self, message: Message) -> tuple[bool, str | None]:
        condition = await self.check_function(message.chat_id)
        if not condition:
            return False, self.fail_message
        return True, None


class IsRegisteredChecker(IChecker):
    def __init__(self, user_service: UserService):
        super().__init__(render_template("fail_is_registered.html"), user_service.is_registered)


class HasPhoneNumberChecker(IChecker):
    def __init__(self, user_service: UserService):
        super().__init__(render_template("fail_has_phone_number.html"), user_service.has_phone)


class HasAccountChecker(IChecker):
    def __init__(self, bank_service: BankService):
        super().__init__(render_template("fail_has_account.html"), bank_service.has_account)


class HasNoAccountChecker(IChecker):
    def __init__(self, bank_service: BankService):
        super().__init__(render_template("fail_has_no_account.html"), bank_service.has_no_account)


class HasCardChecker(IChecker):
    def __init__(self, bank_service: BankService):
        super().__init__(render_template("fail_has_card.html"), bank_service.has_card)
