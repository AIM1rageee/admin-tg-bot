from ninja import Schema


class SuccessResponse(Schema):  # 20x and 30x
    message: str = "Success"


class ErrorResponse(Schema):  # 40x and 50x
    message: str = "Error"
