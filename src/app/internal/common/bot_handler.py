from abc import ABC
from typing import List

from telegram import Update
from telegram.ext import CallbackContext, ConversationHandler

from app.internal.common.checkers import IChecker


class IBotCommandHandler(ABC):
    def __init__(self, checkers: List[IChecker]):
        self.checkers = checkers

    async def check(self, update: Update, context: CallbackContext) -> bool:
        for checker in self.checkers:
            condition, fail_message = await checker.check(update.message)
            if not condition:
                await context.bot.send_message(update.message.chat_id, fail_message, parse_mode="html")
                return False
        return True

    async def cancel_callback(self, update: Update, context: CallbackContext) -> int:
        return ConversationHandler.END
