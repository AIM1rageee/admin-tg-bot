from django.contrib import admin

from app.internal.bank.db.models.account import Account
from app.internal.bank.db.models.card import Card
from app.internal.bank.db.models.operation import Operation


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    pass


@admin.register(Operation)
class OperationAdmin(admin.ModelAdmin):
    pass
