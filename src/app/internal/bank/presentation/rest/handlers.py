from decimal import Decimal
from http import HTTPStatus

from django.db import IntegrityError

from app.internal.bank.domain.entities import AccountOut, CardOut, CardsOut
from app.internal.bank.domain.services import BankService
from app.internal.common.response_entities import ErrorResponse
from config.settings import bank_logger


class BankHandlers:
    def __init__(self, bank_service: BankService):
        self.bank_service = bank_service

    def get_account(self, request, account_id: int):
        account = self.bank_service.get_account_sync(account_id)
        if account is None:
            bank_logger.warning(f"Account with id {account_id} not found")
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)
        bank_logger.debug(f"Account retrieved: account_id={account.account_id}")
        return AccountOut.from_orm(account)

    def create_account(self, request, user_id: int):
        try:
            account = self.bank_service.create_account_sync(user_id)
            bank_logger.info(f"Account created: user_id={user_id}, account_id={account.account_id}")
            return HTTPStatus.CREATED, AccountOut(user_id=user_id, account_id=account.account_id)
        except IntegrityError:
            bank_logger.error(f"Failed to create account for user_id={user_id}")
            return HTTPStatus.BAD_REQUEST, ErrorResponse(message=HTTPStatus.BAD_REQUEST.phrase)

    def get_account_cards(self, request, account_id: int):
        raw_cards = self.bank_service.get_cards_by_account_id(account_id)
        if raw_cards is None:
            bank_logger.warning(f"No cards found for account with id {account_id}")
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)
        bank_logger.debug(f"Cards retrieved for account: account_id={account_id}")
        cards = [CardOut.from_orm(card) for card in raw_cards]
        return CardsOut(cards=cards)

    def get_account_statement(self, request, account_id: int):
        statement = self.bank_service.get_account_statement_sync(account_id)
        if statement is None:
            bank_logger.warning(f"No statement found for account with id {account_id}")
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)
        bank_logger.debug(f"Statement retrieved for account: account_id={account_id}")
        return statement

    def get_card(self, request, card_id: int):
        card = self.bank_service.get_card_sync(card_id)
        if card is None:
            bank_logger.warning(f"Card with id {card_id} not found")
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)
        bank_logger.debug(f"Card retrieved: card_id={card.card_id}")
        return CardOut.from_orm(card)

    def create_card(self, request, account_id: int, balance: Decimal):
        try:
            card = self.bank_service.create_card_sync(account_id, balance)
            bank_logger.info(f"Card created: account_id={account_id}, card_id={card.card_id}")
            return HTTPStatus.CREATED, CardOut.from_orm(card)
        except IntegrityError:
            bank_logger.error(f"Failed to create card for account_id={account_id}")
            return HTTPStatus.BAD_REQUEST, ErrorResponse(message=HTTPStatus.BAD_REQUEST.phrase)
