from decimal import Decimal
from http import HTTPStatus

from ninja import NinjaAPI, Router

from app.internal.bank.domain.entities import AccountOut, CardOut, CardsOut, Statement
from app.internal.bank.presentation.rest.handlers import BankHandlers
from app.internal.common.response_entities import ErrorResponse


def get_accounts_router(bank_handlers: BankHandlers) -> Router:
    router = Router(tags=["accounts"])

    @router.get(
        "/{account_id}",
        response={HTTPStatus.OK: AccountOut, HTTPStatus.NOT_FOUND: ErrorResponse},
    )
    def get_account(request, account_id: int):
        return bank_handlers.get_account(request, account_id)

    @router.post(
        "",
        response={HTTPStatus.CREATED: AccountOut, HTTPStatus.BAD_REQUEST: ErrorResponse},
    )
    def create_account(request, user_id: int):
        return bank_handlers.create_account(request, user_id)

    @router.get(
        "/{account_id}/cards",
        response={HTTPStatus.OK: CardsOut, HTTPStatus.NOT_FOUND: ErrorResponse},
    )
    def get_account_cards(request, account_id: int):
        return bank_handlers.get_account_cards(request, account_id)

    @router.get(
        "/{account_id}/statement",
        response={HTTPStatus.OK: Statement, HTTPStatus.NOT_FOUND: ErrorResponse},
    )
    def get_account_statement(request, account_id: int):
        return bank_handlers.get_account_statement(request, account_id)

    return router


def get_cards_router(bank_handlers: BankHandlers) -> Router:
    router = Router(tags=["cards"])

    @router.get(
        "/{card_id}",
        response={HTTPStatus.OK: CardOut, HTTPStatus.NOT_FOUND: ErrorResponse},
    )
    def get_card(request, card_id: int):
        return bank_handlers.get_card(request, card_id)

    @router.post(
        "",
        response={HTTPStatus.CREATED: CardOut, HTTPStatus.BAD_REQUEST: ErrorResponse},
    )
    def create_card(request, account_id: int, balance: Decimal):
        return bank_handlers.create_card(request, account_id, balance)

    return router


def get_bank_router(bank_handlers: BankHandlers) -> Router:
    router = Router(tags=["bank"])

    accounts_router = get_accounts_router(bank_handlers)
    cards_router = get_cards_router(bank_handlers)

    router.add_router("/accounts", accounts_router)
    router.add_router("/cards", cards_router)

    return router


def add_bank_router(api: NinjaAPI, bank_handlers: BankHandlers):
    bank_router = get_bank_router(bank_handlers)
    api.add_router("/bank", bank_router)
