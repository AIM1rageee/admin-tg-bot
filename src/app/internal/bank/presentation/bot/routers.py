from telegram.ext import Application, CallbackQueryHandler, CommandHandler, ConversationHandler, MessageHandler, filters

from app.internal.bank.domain.services import BankService
from app.internal.bank.presentation.bot.handlers.check_incomes import CheckIncomesHandler
from app.internal.bank.presentation.bot.handlers.deposit import DepositHandler, DepositState
from app.internal.bank.presentation.bot.handlers.interactions import InteractionsHandler
from app.internal.bank.presentation.bot.handlers.open_account import OpenAccountHandler
from app.internal.bank.presentation.bot.handlers.open_card import OpenCardHandler
from app.internal.bank.presentation.bot.handlers.statement import StatementHandler
from app.internal.bank.presentation.bot.handlers.total_money import TotalMoneyHandler
from app.internal.bank.presentation.bot.handlers.transfer_money import TransferMoneyHandler, TransferMoneyState
from app.internal.common.checkers import (
    HasAccountChecker,
    HasCardChecker,
    HasNoAccountChecker,
    HasPhoneNumberChecker,
    IsRegisteredChecker,
)
from app.internal.users.domain.services import UserService


def add_bank_handlers(application: Application, user_service: UserService, bank_service: BankService):
    is_registered_checker = IsRegisteredChecker(user_service)
    has_phone_number_checker = HasPhoneNumberChecker(user_service)
    has_account_checker = HasAccountChecker(bank_service)
    has_no_account_checker = HasNoAccountChecker(bank_service)
    has_card_checker = HasCardChecker(bank_service)

    open_account_handler = OpenAccountHandler(
        bank_service,
        is_registered_checker,
        has_phone_number_checker,
        has_no_account_checker,
    )
    open_card_handler = OpenCardHandler(
        bank_service,
        is_registered_checker,
        has_phone_number_checker,
        has_account_checker,
    )
    deposit_handler = DepositHandler(
        bank_service,
        is_registered_checker,
        has_phone_number_checker,
        has_account_checker,
        has_card_checker,
    )
    interactions_handler = InteractionsHandler(
        bank_service,
        is_registered_checker,
        has_phone_number_checker,
        has_account_checker,
    )
    statement_handler = StatementHandler(
        bank_service,
        is_registered_checker,
        has_phone_number_checker,
        has_account_checker,
    )
    total_money_handler = TotalMoneyHandler(
        bank_service,
        is_registered_checker,
        has_phone_number_checker,
        has_account_checker,
    )
    transfer_money_handler = TransferMoneyHandler(
        user_service,
        bank_service,
        is_registered_checker,
        has_phone_number_checker,
        has_account_checker,
        has_card_checker,
    )
    check_incomes_handler = CheckIncomesHandler(
        bank_service,
        is_registered_checker,
        has_phone_number_checker,
        has_account_checker,
    )

    application.add_handler(
        CommandHandler("open_account", open_account_handler.open_account_callback),
        group=5,
    )
    application.add_handler(
        CommandHandler("open_card", open_card_handler.open_card_callback),
        group=6,
    )
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("deposit", deposit_handler.deposit_callback)],
            states={
                DepositState.MONEY_INPUT: [
                    MessageHandler(filters.TEXT & (~filters.COMMAND), deposit_handler.money_input_callback),
                ],
            },
            fallbacks=[MessageHandler(filters.COMMAND, deposit_handler.cancel_callback)],
            per_user=True,
            per_chat=True,
            allow_reentry=True,
        ),
        group=7,
    )
    application.add_handler(
        CommandHandler("interactions", interactions_handler.interactions_callback),
        group=8,
    )
    application.add_handler(
        CommandHandler("statement", statement_handler.statement_callback),
        group=9,
    )
    application.add_handler(CommandHandler("total_money", total_money_handler.total_money_callback), group=10)
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("transfer_money", transfer_money_handler.transfer_money_callback)],
            states={
                TransferMoneyState.TRANSFER_OPTION_CHOICE: [
                    CallbackQueryHandler(transfer_money_handler.transfer_by_card_callback, pattern=r"^card"),
                    CallbackQueryHandler(transfer_money_handler.transfer_by_username_callback, pattern=r"^username"),
                ],
                TransferMoneyState.TRANSFER_BY_CARD: [
                    MessageHandler(
                        filters.TEXT & (~filters.COMMAND) & filters.Regex(r"^[0-9]+\s+\d+"),
                        transfer_money_handler.card_input_callback,
                    ),
                ],
                TransferMoneyState.TRANSFER_BY_USERNAME: [
                    MessageHandler(
                        filters.TEXT & (~filters.COMMAND) & filters.Regex(r"^[a-zA-Z0-9_]+\s+\d+"),
                        transfer_money_handler.username_input_callback,
                    ),
                ],
                TransferMoneyState.CARD_CHOICE: [
                    CallbackQueryHandler(transfer_money_handler.card_choice_callback, pattern=r"^transfer \d+ \d+ \d+"),
                ],
                TransferMoneyState.TRANSFER_CONFIRMATION: [
                    MessageHandler(filters.TEXT | filters.PHOTO, transfer_money_handler.transfer_confirmation_callback),
                ],
            },
            fallbacks=[MessageHandler(filters.COMMAND, transfer_money_handler.cancel_callback)],
            per_user=True,
            per_chat=True,
            allow_reentry=True,
        ),
        group=11,
    )
    application.add_handler(
        CommandHandler("check_incomes", check_incomes_handler.check_incomes_callback),
        group=12,
    )
