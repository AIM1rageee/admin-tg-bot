from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank.domain.services import BankService
from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import HasNoAccountChecker, HasPhoneNumberChecker, IsRegisteredChecker
from app.template_engine import render_template
from config.settings import bank_logger


class OpenAccountHandler(IBotCommandHandler):
    def __init__(
        self,
        bank_service: BankService,
        is_registered_checker: IsRegisteredChecker,
        has_phone_number_checker: HasPhoneNumberChecker,
        has_no_account_checker: HasNoAccountChecker,
    ):
        super().__init__([is_registered_checker, has_phone_number_checker, has_no_account_checker])
        self.bank_service = bank_service

    async def open_account_callback(self, update: Update, context: CallbackContext):
        if not await self.check(update, context):
            return
        account = await self.bank_service.create_account(update.message.chat_id)
        await context.bot.send_message(
            update.message.chat_id,
            render_template("account_opened.html", account=account),
            parse_mode="html",
        )
        bank_logger.info(f"Opened account for user {update.message.chat_id}")
