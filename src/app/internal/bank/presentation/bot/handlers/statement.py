from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank.domain.services import BankService
from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import HasAccountChecker, HasPhoneNumberChecker, IsRegisteredChecker
from app.template_engine import render_template
from config.settings import bank_logger


class StatementHandler(IBotCommandHandler):
    def __init__(
        self,
        bank_service: BankService,
        is_registered_checker: IsRegisteredChecker,
        has_phone_number: HasPhoneNumberChecker,
        has_account_checker: HasAccountChecker,
    ):
        super().__init__([is_registered_checker, has_phone_number, has_account_checker])
        self.bank_service = bank_service

    async def statement_callback(self, update: Update, context: CallbackContext):
        if not await self.check(update, context):
            return
        statement = await self.bank_service.get_account_statement_by_telegram_id(update.message.chat_id)
        await context.bot.send_message(
            update.message.chat_id,
            render_template(
                "account_statement.html",
                operations_sent=statement.sent,
                operations_received=statement.received,
                account_id=statement.account_id,
                total_amount=statement.total_amount,
            ),
            parse_mode="html",
        )
        bank_logger.info(f"Sent account statement to user {update.message.chat_id}")
