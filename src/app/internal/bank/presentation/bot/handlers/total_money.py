from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank.domain.services import BankService
from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import HasAccountChecker, HasPhoneNumberChecker, IsRegisteredChecker
from app.template_engine import render_template
from config.settings import bank_logger


class TotalMoneyHandler(IBotCommandHandler):
    def __init__(
        self,
        bank_service: BankService,
        is_registered_checker: IsRegisteredChecker,
        has_phone_number: HasPhoneNumberChecker,
        has_account_checker: HasAccountChecker,
    ):
        super().__init__([is_registered_checker, has_phone_number, has_account_checker])
        self.bank_service = bank_service

    async def total_money_callback(self, update: Update, context: CallbackContext):
        if not await self.check(update, context):
            return
        cards = await self.bank_service.get_cards(update.message.chat_id)
        total_money = sum(float(c["balance"]) for c in cards)
        await context.bot.send_message(
            update.message.chat_id,
            render_template("account_info.html", cards=cards, total_money=total_money),
            parse_mode="html",
        )
        bank_logger.info(f"Sent total money info to user {update.message.chat_id}")
