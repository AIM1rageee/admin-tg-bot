from enum import IntEnum

from django.db import OperationalError
from telegram import Update
from telegram.ext import CallbackContext, ConversationHandler

from app.internal.bank.domain.services import BankService
from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import HasAccountChecker, HasCardChecker, HasPhoneNumberChecker, IsRegisteredChecker
from app.template_engine import render_template
from config.settings import bank_logger


class DepositState(IntEnum):
    MONEY_INPUT = 0


class DepositHandler(IBotCommandHandler):
    def __init__(
        self,
        bank_service: BankService,
        is_registered_checker: IsRegisteredChecker,
        has_phone_number: HasPhoneNumberChecker,
        has_account_checker: HasAccountChecker,
        has_card_checker: HasCardChecker,
    ):
        super().__init__([is_registered_checker, has_phone_number, has_account_checker, has_card_checker])
        self.bank_service = bank_service

    async def deposit_callback(self, update: Update, context: CallbackContext) -> int:
        if not await self.check(update, context):
            return ConversationHandler.END
        await context.bot.send_message(update.message.chat_id, render_template("money_input.html"), parse_mode="html")
        bank_logger.info(f"User {update.message.chat_id} started the deposit process")
        return DepositState.MONEY_INPUT

    async def money_input_callback(self, update: Update, context: CallbackContext) -> int:
        try:
            amount = update.message.text
            random_card = await self.bank_service.deposit_random_card(update.message.chat_id, amount)
            await context.bot.send_message(
                update.message.chat_id,
                render_template("money_income.html", card=random_card),
                parse_mode="html",
            )
            bank_logger.info(f"Successfully deposited {amount} to random card for user {update.message.chat_id}")
            return ConversationHandler.END
        except OperationalError as e:
            await context.bot.send_message(update.message.chat_id, f"❌{e}")
            bank_logger.error(f"Error during deposit for user {update.message.chat_id}: {e}")
            return DepositState.MONEY_INPUT
