from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank.domain.services import BankService
from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import HasAccountChecker, HasPhoneNumberChecker, IsRegisteredChecker
from app.template_engine import render_template
from config.settings import bank_logger


class CheckIncomesHandler(IBotCommandHandler):
    def __init__(
        self,
        bank_service: BankService,
        is_registered_checker: IsRegisteredChecker,
        has_phone_number: HasPhoneNumberChecker,
        has_account_checker: HasAccountChecker,
    ):
        super().__init__([is_registered_checker, has_phone_number, has_account_checker])
        self.bank_service = bank_service

    async def check_incomes_callback(self, update: Update, context: CallbackContext):
        received = await self.bank_service.get_not_seen_incomes_by_telegram_id(update.message.chat_id)
        for receive_info in received:
            await context.bot.send_message(
                update.message.chat_id,
                render_template("receive_info.html", operation=receive_info),
                parse_mode="html",
            )
            bank_logger.info(f"Sent receive_info message to user {update.message.chat_id}")
        if not received:
            await context.bot.send_message(update.message.chat_id, "🙈Новых поступлений пока нет!")
            bank_logger.info(f"No new incomes received for user {update.message.chat_id}")
