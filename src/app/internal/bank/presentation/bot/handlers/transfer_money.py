from enum import IntEnum

from django.core.files.base import ContentFile
from django.db import OperationalError
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import CallbackContext, ConversationHandler

from app.internal.bank.domain.services import BankService
from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import HasAccountChecker, HasCardChecker, HasPhoneNumberChecker, IsRegisteredChecker
from app.internal.users.domain.services import UserService
from config.settings import bank_logger


class TransferMoneyState(IntEnum):
    TRANSFER_OPTION_CHOICE = 0
    TRANSFER_BY_CARD = 1
    TRANSFER_BY_USERNAME = 2
    CARD_CHOICE = 3
    TRANSFER_CONFIRMATION = 4


class TransferMoneyHandler(IBotCommandHandler):
    def __init__(
        self,
        user_service: UserService,
        bank_service: BankService,
        is_registered_checker: IsRegisteredChecker,
        has_phone_number: HasPhoneNumberChecker,
        has_account_checker: HasAccountChecker,
        has_card_checker: HasCardChecker,
    ):
        super().__init__([is_registered_checker, has_phone_number, has_account_checker, has_card_checker])
        self.user_service = user_service
        self.bank_service = bank_service

    async def transfer_money_callback(self, update: Update, context: CallbackContext) -> int:
        if not await self.check(update, context):
            return ConversationHandler.END
        await update.message.reply_text(
            "Выберите способ перевода:",
            reply_markup=self.generate_card_transfer_option_markup(),
        )
        return TransferMoneyState.TRANSFER_OPTION_CHOICE

    async def transfer_by_card_callback(self, update: Update, context: CallbackContext) -> int:
        query = update.callback_query
        await query.answer()

        await query.edit_message_text("➡️Введите номер карты в формате Номер Сумма:")
        return TransferMoneyState.TRANSFER_BY_CARD

    async def card_input_callback(self, update: Update, context: CallbackContext) -> int:
        card_id, amount = map(int, update.message.text.split())
        await update.message.reply_text(
            "Выберите карту для перевода:",
            reply_markup=await self.generate_cards_choice_markup(update.message.chat_id, card_id, amount),
        )

        return TransferMoneyState.CARD_CHOICE

    async def transfer_by_username_callback(self, update: Update, context: CallbackContext) -> int:
        query = update.callback_query
        await query.answer()

        await query.edit_message_text("➡️Введите имя пользователя в формате Имя Сумма:")
        return TransferMoneyState.TRANSFER_BY_USERNAME

    async def username_input_callback(self, update: Update, context: CallbackContext) -> int:
        user = await self.user_service.get_user_by_username(update.message.text.split()[0])
        amount = int(update.message.text.split()[1])
        if user is None:
            await context.bot.send_message(update.message.chat_id, "❌Такого пользователя не существует!")
            return TransferMoneyState.TRANSFER_BY_USERNAME
        random_card = await self.bank_service.get_random_user_card(user.telegram_id)
        if random_card is None:
            await context.bot.send_message(update.message.chat_id, "❌У пользователя нет карт!")
            return TransferMoneyState.TRANSFER_BY_USERNAME

        await update.message.reply_text(
            "Выберите карту для перевода, на которой достаточно средств:",
            reply_markup=await self.generate_cards_choice_markup(update.message.chat_id, random_card.card_id, amount),
        )

        return TransferMoneyState.CARD_CHOICE

    async def card_choice_callback(self, update: Update, context: CallbackContext) -> int:
        query = update.callback_query
        await query.answer()

        sender_card_id, receiver_card_id, amount = map(int, query.data.split()[1:])
        context.chat_data["sender_card_id"] = sender_card_id
        context.chat_data["receiver_card_id"] = receiver_card_id
        context.chat_data["amount"] = amount
        await query.edit_message_text("💌Подтвердите перевод! Можете отправить открытку, сообщение или всё сразу :)")
        return TransferMoneyState.TRANSFER_CONFIRMATION

    async def transfer_confirmation_callback(self, update: Update, context: CallbackContext) -> int:
        sender_card_id = context.chat_data["sender_card_id"]
        receiver_card_id = context.chat_data["receiver_card_id"]
        amount = context.chat_data["amount"]
        image = (
            ContentFile(
                name=update.message.photo[-1].file_id,
                content=await (await update.message.photo[-1].get_file()).download_as_bytearray(),
            )
            if len(update.message.photo) > 0
            else None
        )
        try:
            await self.bank_service.transfer_money_from_card_to_card(sender_card_id, receiver_card_id, amount, image)
            await context.bot.send_message(update.message.chat_id, "✅Перевод успешно отправлен!")
            bank_logger.info(
                f"Money transfer successfully completed: from {sender_card_id} to {receiver_card_id}, amount: {amount}"
            )
        except OperationalError as e:
            bank_logger.error(f"Error occurred during money transfer: {e}")
            await context.bot.send_message(update.message.chat_id, f"❌{e}")
        return ConversationHandler.END

    def generate_card_transfer_option_markup(self) -> InlineKeyboardMarkup:
        keyboard = [
            [
                InlineKeyboardButton("💳По карте", callback_data="card"),
                InlineKeyboardButton("😎По никнейму", callback_data="username"),
            ],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        return reply_markup

    async def generate_cards_choice_markup(
        self,
        sender_user_id: int,
        receiver_card_id: int,
        amount,
    ) -> InlineKeyboardMarkup:
        cards = await self.bank_service.get_cards(sender_user_id)
        keyboard = [
            [
                InlineKeyboardButton(
                    f"💳{card['card_id']}: {card['balance']}",
                    callback_data=f"transfer {card['card_id']} {receiver_card_id} {amount}",
                ),
            ]
            for card in cards
        ]
        return InlineKeyboardMarkup(keyboard)
