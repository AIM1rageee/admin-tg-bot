from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank.domain.services import BankService
from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import HasAccountChecker, HasPhoneNumberChecker, IsRegisteredChecker
from app.template_engine import render_template
from config.settings import bank_logger


class InteractionsHandler(IBotCommandHandler):
    def __init__(
        self,
        bank_service: BankService,
        is_registered_checker: IsRegisteredChecker,
        has_phone_number: HasPhoneNumberChecker,
        has_account_checker: HasAccountChecker,
    ):
        super().__init__([is_registered_checker, has_phone_number, has_account_checker])
        self.bank_service = bank_service

    async def interactions_callback(self, update: Update, context: CallbackContext):
        if not await self.check(update, context):
            return
        usernames = await self.bank_service.get_interacted_usernames(update.message.chat_id)
        await context.bot.send_message(
            update.message.chat_id,
            render_template("interactions_info.html", usernames=usernames),
            parse_mode="html",
        )
        bank_logger.info(f"Sent interactions info to user {update.message.chat_id}")
