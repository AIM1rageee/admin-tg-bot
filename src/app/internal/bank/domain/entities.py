from datetime import datetime
from decimal import Decimal
from typing import List, Optional

from ninja import Schema


class AccountOut(Schema):
    user_id: int
    account_id: int


class CardOut(Schema):
    card_id: int
    balance: Decimal


class CardsOut(Schema):
    cards: List[CardOut]


class OperationOut(Schema):
    sender_account_id: int
    receiver_account_id: int
    done: bool
    amount: Decimal
    operation_time: datetime


class ReceivedInfo(Schema):
    received_from: int
    done: bool
    amount: Decimal
    operation_time: datetime
    postcard_url: Optional[str]


class SentInfo(Schema):
    sent_to: int
    done: bool
    amount: Decimal
    operation_time: datetime


class Statement(Schema):
    account_id: int
    received: List[ReceivedInfo]
    sent: List[SentInfo]
    total_amount: Decimal
