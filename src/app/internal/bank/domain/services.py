from decimal import Decimal, InvalidOperation
from typing import List, Optional

from asgiref.sync import sync_to_async
from django.core.files.base import ContentFile
from django.db import OperationalError, transaction
from django.db.models import F
from prometheus_client.metrics import Counter, Summary

from app.internal.bank.db.models.account import Account
from app.internal.bank.db.models.card import Card
from app.internal.bank.db.models.operation import Operation
from app.internal.bank.domain.entities import ReceivedInfo, SentInfo, Statement
from app.internal.bank.domain.utils import get_full_url, get_month_bounds, get_total_amount, save_image
from app.internal.users.db.models import TgUser
from config.settings import bank_logger

TRANSFERRED_MONEY = Summary("total_transferred_money", "Money turnover (transferred money)")
MONEY_RECEIVED_AS_GIFT = Summary("money_received_as_gift", "Money received as gift")
OPENED_CARDS_COUNTER = Counter("opened_cards_count", "Opened cards count in application")
OPENED_ACCOUNT_COUNTER = Counter("opened_accounts_count", "Opened accounts count in application")
REQUEST_MONEY_TRANSFER_TIME = Summary("request_money_transfer_time", "Request money transfer time")
SENT_POSTCARDS_COUNT = Counter("sent_postcards_count", "Sent postcards count")


class BankService:
    ERROR_INVALID_MONEY_INPUT = "Сумма указана неверно!"
    ERROR_INVALID_TRANSFER = "Ошибка перевода!"
    ERROR_NON_POSITIVE_AMOUNT = "Сумма должна быть положительной!"
    ERROR_INSUFFICIENT_FUNDS = "Недостаточно средств на карте!"
    ERROR_MAX_DEPOSIT_VALUE_REACHED = "Превышена максимальная сумма для перевода!"
    MAX_DEPOSIT_VALUE = Decimal("1e16")

    def get_account_sync(self, account_id) -> Account:
        bank_logger.debug(f"Getting account with ID {account_id}")
        return Account.objects.filter(account_id=account_id).first()

    def get_cards_by_account_id(self, account_id: int) -> Optional[List[Card]]:
        if not Account.objects.filter(pk=account_id).exists():
            bank_logger.warning(f"Cards for account with ID {account_id} do not exist")
            return None
        bank_logger.debug(f"Getting cards for account with ID {account_id}")
        return list(Card.objects.filter(account_id=account_id))

    async def create_account(self, telegram_id) -> Account:
        return await sync_to_async(self.create_account_sync)(telegram_id)

    def create_account_sync(self, telegram_id) -> Account:
        bank_logger.debug(f"Creating account for user with Telegram ID {telegram_id}")
        OPENED_ACCOUNT_COUNTER.inc()
        return Account.objects.create(user_id=telegram_id)

    async def has_account(self, telegram_id) -> bool:
        return await sync_to_async(self.has_account_sync)(telegram_id)

    async def has_no_account(self, telegram_id) -> bool:
        return not await self.has_account(telegram_id)

    def has_account_sync(self, telegram_id) -> bool:
        bank_logger.debug(f"Checking account for user with Telegram ID {telegram_id}")
        return Account.objects.filter(user_id=telegram_id).exists()

    async def get_interacted_usernames(self, telegram_id) -> list[str]:
        return await sync_to_async(self.get_interacted_usernames_sync)(telegram_id)

    def get_interacted_usernames_sync(self, telegram_id) -> list[str]:
        username, account_id = TgUser.objects.values_list("username", "account__account_id").get(
            telegram_id=telegram_id,
        )
        received_from_usernames_query = (
            TgUser.objects.filter(account__send_operations__receiver_account_id=account_id)
            .exclude(username=username)
            .values_list("username", flat=True)
        )
        sent_to_usernames_query = (
            TgUser.objects.filter(account__receive_operations__sender_account_id=account_id)
            .exclude(username=username)
            .values_list("username", flat=True)
        )
        bank_logger.debug(f"Getting interacted usernames for user with Telegram ID {telegram_id}")
        return list(received_from_usernames_query.union(sent_to_usernames_query))

    async def get_account_statement_by_telegram_id(self, telegram_id) -> Optional[Statement]:
        return await sync_to_async(self.get_account_statement_by_telegram_id_sync)(telegram_id)

    def get_account_statement_sync(self, account_id: int, check_account: bool = True) -> Optional[Statement]:
        if check_account and not Account.objects.filter(account_id=account_id).exists():
            bank_logger.warning(f"Account with ID {account_id} does not exist")
            return None
        start_of_month, end_of_month = get_month_bounds()
        received = [
            ReceivedInfo(
                received_from=r["sender_account_id"],
                done=r["done"],
                amount=r["amount"],
                operation_time=r["operation_time"],
                postcard_url=get_full_url(r["postcard"]),
            )
            for r in Operation.objects.filter(
                receiver_account_id=account_id,
                operation_time__gte=start_of_month,
                operation_time__lt=end_of_month,
            )
            .exclude(sender_account_id=account_id)
            .order_by("operation_time")
            .values("sender_account_id", "amount", "done", "operation_time", "postcard")
        ]
        sent = [
            SentInfo(
                sent_to=s["receiver_account_id"],
                done=s["done"],
                amount=s["amount"],
                operation_time=s["operation_time"],
            )
            for s in Operation.objects.filter(
                sender_account_id=account_id,
                operation_time__gte=start_of_month,
                operation_time__lt=end_of_month,
            )
            .exclude(receiver_account_id=account_id)
            .order_by("operation_time")
            .values("receiver_account_id", "amount", "done", "operation_time")
        ]
        total_amount = get_total_amount(received, sent)
        bank_logger.debug(f"Getting statement for account with ID {account_id}")
        return Statement(
            account_id=account_id,
            received=received,
            sent=sent,
            total_amount=total_amount,
        )

    def get_account_statement_by_telegram_id_sync(self, telegram_id) -> Optional[Statement]:
        account_id = Account.objects.values_list("account_id", flat=True).filter(user_id=telegram_id).first()
        bank_logger.debug(f"Getting statement for user with Telegram ID {telegram_id}")
        return None if account_id is None else self.get_account_statement_sync(account_id, False)

    async def create_card_by_telegram_id(self, telegram_id: int, start_balance: Decimal = Decimal(5000)) -> Card:
        return await sync_to_async(self.create_card_by_telegram_id_sync)(telegram_id, start_balance)

    def create_card_by_telegram_id_sync(self, telegram_id: int, start_balance: Decimal = Decimal(5000)) -> Card:
        account_id = Account.objects.values_list("account_id", flat=True).get(user_id=telegram_id)
        card = Card.objects.create(account_id=account_id, balance=start_balance)
        bank_logger.debug(f"Creating card for user with Telegram ID {telegram_id}")
        return card

    def create_card_sync(self, account_id: int, start_balance: Decimal):
        card = Card.objects.create(account_id=account_id, balance=start_balance)
        bank_logger.debug(f"Creating card for account with ID {account_id}")
        OPENED_CARDS_COUNTER.inc()
        MONEY_RECEIVED_AS_GIFT.observe(float(start_balance))
        return card

    async def get_card(self, card_id) -> Optional[Card]:
        return await sync_to_async(self.get_card_sync)(card_id)

    def get_card_sync(self, card_id) -> Optional[Card]:
        bank_logger.debug(f"Getting card with ID {card_id}")
        return Card.objects.filter(card_id=card_id).first()

    async def has_card(self, telegram_id) -> bool:
        return await sync_to_async(self.has_card_sync)(telegram_id)

    def has_card_sync(self, telegram_id) -> bool:
        bank_logger.debug(f"Checking user with Telegram ID {telegram_id} has any card")
        return Card.objects.filter(account__user_id=telegram_id).exists()

    async def get_cards(self, telegram_id) -> list:
        return await sync_to_async(self.get_cards_sync)(telegram_id)

    def get_cards_sync(self, telegram_id) -> list:
        bank_logger.debug(f"Getting cards for user with Telegram ID {telegram_id}")
        return list(Card.objects.filter(account__user_id=telegram_id).order_by("-balance").values("card_id", "balance"))

    async def get_random_user_card(self, telegram_id) -> Optional[Card]:
        return await sync_to_async(self.get_random_user_card_sync)(telegram_id)

    def get_random_user_card_sync(self, telegram_id) -> Optional[Card]:
        bank_logger.debug(f"Getting random card for user with Telegram ID {telegram_id}")
        return Card.objects.filter(account__user_id=telegram_id).order_by("?").first()

    async def deposit_random_card(self, telegram_id, amount) -> Card:
        return await sync_to_async(self.deposit_random_user_card_sync)(telegram_id, amount)

    def deposit_random_user_card_sync(self, telegram_id, amount) -> Card:
        try:
            amount = Decimal(amount)
            random_card = self.get_random_user_card_sync(telegram_id)
            if random_card is None:
                raise OperationalError(BankService.ERROR_INVALID_TRANSFER)
            if amount <= 0:
                raise OperationalError(BankService.ERROR_NON_POSITIVE_AMOUNT)
            if amount > BankService.MAX_DEPOSIT_VALUE:
                raise OperationalError(BankService.ERROR_MAX_DEPOSIT_VALUE_REACHED)
            random_card.balance = F("balance") + amount
            random_card.save(update_fields=("balance",))
            bank_logger.debug(f"Deposit random card for user with Telegram ID {telegram_id}")
            return random_card
        except InvalidOperation:
            raise OperationalError(BankService.ERROR_INVALID_MONEY_INPUT)

    async def transfer_money_from_card_to_card(
        self,
        sender_card_id,
        receiver_card_id,
        amount,
        image: ContentFile,
    ) -> None:
        await sync_to_async(self.transfer_money_from_card_to_card_sync)(sender_card_id, receiver_card_id, amount, image)

    @REQUEST_MONEY_TRANSFER_TIME.time()
    def transfer_money_from_card_to_card_sync(
        self,
        sender_card_id,
        receiver_card_id,
        amount,
        image: ContentFile,
    ) -> None:
        try:
            with transaction.atomic():
                amount = Decimal(amount)
                sender_card = self.get_card_sync(sender_card_id)
                receiver_card = self.get_card_sync(receiver_card_id)
                if sender_card is None or receiver_card is None or sender_card_id == receiver_card_id:
                    raise OperationalError(BankService.ERROR_INVALID_TRANSFER)
                if amount <= 0:
                    raise OperationalError(BankService.ERROR_NON_POSITIVE_AMOUNT)
                if amount > BankService.MAX_DEPOSIT_VALUE:
                    raise OperationalError(BankService.ERROR_MAX_DEPOSIT_VALUE_REACHED)
                if sender_card.balance < amount:
                    raise OperationalError(BankService.ERROR_INSUFFICIENT_FUNDS)
                path = save_image(image)
                sender_card.balance = F("balance") - amount
                sender_card.save(update_fields=("balance",))
                receiver_card.balance = F("balance") + amount
                receiver_card.save(update_fields=("balance",))
                Operation.objects.create(
                    sender_account=sender_card.account,
                    receiver_account=receiver_card.account,
                    amount=amount,
                    done=True,
                    postcard=path,
                )
                bank_logger.debug(
                    f"Successful money transfer from card {sender_card_id} to card {receiver_card_id} "
                    f"with amount {amount}"
                )
                if path is not None:
                    SENT_POSTCARDS_COUNT.inc()
                TRANSFERRED_MONEY.inc(float(amount))
        except Exception as e:
            bank_logger.error(
                f"Failed to transfer money {amount} from card {sender_card_id} to card {receiver_card_id} "
                f"with amount {amount}"
            )
            if sender_card is not None and receiver_card is not None:
                Operation.objects.create(
                    sender_account=sender_card.account,
                    receiver_account=receiver_card.account,
                    amount=amount,
                )
            raise e

    async def get_not_seen_incomes_by_telegram_id(self, telegram_id: int) -> List[ReceivedInfo]:
        return await sync_to_async(self.get_not_seen_incomes_by_telegram_id_sync)(telegram_id)

    def get_not_seen_incomes_by_telegram_id_sync(self, telegram_id: int) -> List[ReceivedInfo]:
        account_id = Account.objects.values_list("account_id").get(user_id=telegram_id)
        bank_logger.debug(f"Getting not seen incomes for user with Telegram ID {telegram_id}")
        return self.get_not_seen_incomes_sync(account_id)

    def get_not_seen_incomes_sync(self, account_id: int) -> List[ReceivedInfo]:
        filtering_query = Operation.objects.filter(receiver_account_id=account_id, seen=False).exclude(
            sender_account_id=account_id,
        )
        received = [
            ReceivedInfo(
                received_from=r["sender_account_id"],
                done=r["done"],
                amount=r["amount"],
                operation_time=r["operation_time"],
                postcard_url=get_full_url(r["postcard"]),
            )
            for r in filtering_query.order_by("operation_time").values(
                "sender_account_id",
                "amount",
                "done",
                "operation_time",
                "postcard",
            )
        ]
        filtering_query.update(seen=True)
        bank_logger.debug(f"Getting not seen incomes for account with ID {account_id}")
        return received
