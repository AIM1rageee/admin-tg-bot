from datetime import datetime, timedelta
from decimal import Decimal
from typing import List, Optional

from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.utils.timezone import now

from app.internal.bank.domain.entities import ReceivedInfo, SentInfo
from config.settings import AWS_S3_ENDPOINT_URL, AWS_STORAGE_BUCKET_NAME


def get_month_bounds() -> tuple[datetime, datetime]:
    start_of_month = now().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
    end_of_month = (start_of_month + timedelta(days=32)).replace(day=1, hour=0, minute=0, second=0, microsecond=0)
    return start_of_month, end_of_month


def get_total_amount(received: List[ReceivedInfo], sent: List[SentInfo]) -> Decimal:
    total_amount = Decimal(0)
    for operation in received:
        total_amount += operation.amount
    for operation in sent:
        total_amount -= operation.amount
    return total_amount


def save_image(image: ContentFile) -> Optional[str]:
    if not image:
        return None
    return default_storage.save(f"postcards/{image.name}.jpg", image)


def get_full_url(relative_path: Optional[str]) -> Optional[str]:
    if not relative_path:
        return None
    return "/".join([AWS_S3_ENDPOINT_URL, AWS_STORAGE_BUCKET_NAME, relative_path])
