from django.db import models

from app.internal.users.db.models import TgUser


class Account(models.Model):
    account_id = models.AutoField(primary_key=True)
    user = models.OneToOneField(TgUser, on_delete=models.RESTRICT, related_name="account", db_index=True)

    def __str__(self):
        return f"{self.account_id} account"

    class Meta:
        verbose_name = "Bank Account"
        verbose_name_plural = "Bank Accounts"
