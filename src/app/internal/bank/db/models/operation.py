from django.core.validators import MinValueValidator
from django.db import models

from app.internal.bank.db.models.account import Account


class Operation(models.Model):
    sender_account = models.ForeignKey(Account, null=True, related_name="send_operations", on_delete=models.SET_NULL)
    receiver_account = models.ForeignKey(
        Account,
        null=True,
        related_name="receive_operations",
        on_delete=models.SET_NULL,
    )
    done = models.BooleanField(null=False, default=False)
    amount = models.DecimalField(
        verbose_name="amount",
        max_digits=50,
        decimal_places=2,
        validators=[MinValueValidator(0, "Сумма должна быть неотрицательной!")],
    )
    operation_time = models.DateTimeField(auto_now_add=True)
    postcard = models.ImageField(upload_to="postcards/", null=True, default=None)
    seen = models.BooleanField(null=False, default=False)

    def __str__(self):
        return f"Receive {self.amount} rubles from {self.sender_account} to {self.receiver_account}"

    class Meta:
        verbose_name = "Operation"
        verbose_name_plural = "Operations"
        indexes = [
            models.Index(
                fields=["sender_account_id", "receiver_account_id", "operation_time", "amount"],
                name="sen_rec_acc_id_optime_amount",
            ),
            models.Index(
                fields=["receiver_account_id", "sender_account_id", "operation_time", "amount"],
                name="rec_sen_acc_id_optime_amount",
            ),
        ]
