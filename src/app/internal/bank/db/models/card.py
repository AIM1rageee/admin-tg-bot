from django.core.validators import MinValueValidator
from django.db import models

from app.internal.bank.db.models.account import Account


class Card(models.Model):
    card_id = models.AutoField(primary_key=True)
    account = models.ForeignKey(Account, on_delete=models.RESTRICT, related_name="cards", db_index=True)
    balance = models.DecimalField(
        verbose_name="balance",
        max_digits=50,
        decimal_places=2,
        validators=[MinValueValidator(0, "Сумма должна быть неотрицательной!")],
    )

    def __str__(self):
        return f"Card {self.card_id} connected to {self.account}"

    class Meta:
        verbose_name = "Card"
        verbose_name_plural = "Cards"
