import logging

from telegram.ext import Application, ApplicationBuilder

from app.internal.bank.domain.services import BankService
from app.internal.bank.presentation.bot.routers import add_bank_handlers
from app.internal.users.domain.services import UserService
from app.internal.users.presentation.bot.routers import add_users_handlers
from config.settings import TG_TOKEN

logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
logging.getLogger("httpx").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)


def build_application() -> Application:
    application: Application = ApplicationBuilder().token(TG_TOKEN).build()

    user_service = UserService()
    add_users_handlers(application, user_service)

    bank_service = BankService()
    add_bank_handlers(application, user_service, bank_service)

    return application
