from typing import List

from asgiref.sync import sync_to_async
from phonenumbers import is_possible_number_string

from app.internal.users.db.models import TgUser
from app.internal.users.domain.entities import TgUserIn


class UserService:
    def update_user(self, telegram_id: int, data: TgUserIn) -> None:
        TgUser.objects.filter(telegram_id=telegram_id).update(**data.dict())

    async def try_update_user_phone(self, telegram_id: int, phone: str) -> bool:
        return await sync_to_async(self.try_update_user_phone_sync)(telegram_id, phone)

    def try_update_user_phone_sync(self, telegram_id: int, phone: str) -> bool:
        if is_possible_number_string(phone, "ru"):
            TgUser.objects.filter(telegram_id=telegram_id).update(phone=phone)
            return True
        return False

    async def create_user(self, telegram_id: int, data: TgUserIn) -> TgUser:
        return await sync_to_async(self.create_user_sync)(telegram_id, data)

    def create_user_sync(self, telegram_id: int, data: TgUserIn) -> TgUser:
        user, created = TgUser.objects.update_or_create(
            telegram_id=telegram_id,
            defaults={
                "username": data.username,
                "first_name": data.first_name,
                "last_name": data.last_name,
            },
        )
        return user

    async def is_registered(self, telegram_id) -> bool:
        return await sync_to_async(self.is_registered_sync)(telegram_id)

    def is_registered_sync(self, telegram_id) -> bool:
        return TgUser.objects.filter(telegram_id=telegram_id).exists()

    async def has_phone(self, telegram_id) -> bool:
        return await sync_to_async(self.has_phone_sync)(telegram_id)

    def has_phone_sync(self, telegram_id) -> bool:
        return TgUser.objects.filter(telegram_id=telegram_id).exclude(phone__isnull=True).exists()

    async def get_user(self, telegram_id) -> TgUser | None:
        return await sync_to_async(self.get_user_sync)(telegram_id)

    def get_user_sync(self, telegram_id) -> TgUser | None:
        return TgUser.objects.filter(telegram_id=telegram_id).first()

    async def get_user_by_username(self, username) -> TgUser | None:
        return await sync_to_async(self.get_user_by_username_sync)(username)

    def get_user_by_username_sync(self, username) -> TgUser | None:
        return TgUser.objects.filter(username=username).first()

    async def get_favorites(self, telegram_id) -> List[str]:
        return await sync_to_async(self.get_favorites_sync)(telegram_id)

    def get_favorites_sync(self, telegram_id) -> List[str]:
        favorites = list(
            TgUser.objects.filter(telegram_id=telegram_id).values_list(
                "favorite_users__username",
                flat=True,
            ),
        )
        if len(favorites) == 1 and favorites[0] is None:
            return list()
        return favorites

    async def try_add_favorite(self, user_id, favorite_username) -> bool:
        return await sync_to_async(self.try_add_favorite_sync)(user_id, favorite_username)

    def try_add_favorite_sync(self, user_id, favorite_username) -> bool:
        user = self.get_user_sync(user_id)
        favorite = self.get_user_by_username_sync(favorite_username)
        if user is None or favorite is None:
            return False
        user.favorite_users.add(favorite)
        return True

    async def try_remove_favorite(self, user_id: int, favorite_username: str) -> bool:
        return await sync_to_async(self.try_remove_favorite_sync)(user_id, favorite_username)

    def try_remove_favorite_sync(self, user_id: int, favorite_username: str) -> bool:
        user = self.get_user_sync(user_id)
        favorite = self.get_user_by_username_sync(favorite_username)
        if user is None or favorite is None:
            return False
        user.favorite_users.remove(favorite)
        return True
