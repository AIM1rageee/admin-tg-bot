from typing import List

from ninja import ModelSchema, Schema

from app.models import TgUser


class TgUserIn(ModelSchema):
    class Meta:
        model = TgUser
        fields = ["username", "first_name", "last_name", "phone"]


class TgUserOut(ModelSchema):
    class Meta:
        model = TgUser
        fields = ["telegram_id", "username", "first_name", "last_name", "phone"]


class TgUserPhoneIn(ModelSchema):
    class Meta:
        model = TgUser
        fields = ["phone"]


class FavoriteUsersOut(Schema):
    favorite_users: List[str]


class FavoriteUserIn(Schema):
    username: str
