from django.db import models


class TgUser(models.Model):
    telegram_id = models.BigIntegerField(primary_key=True)
    username = models.CharField(max_length=50, db_index=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, null=True)
    phone = models.CharField(max_length=20, null=True)
    favorite_users = models.ManyToManyField("self", symmetrical=False)

    @property
    def full_name(self):
        if self.last_name is None:
            return self.first_name
        return str(self.first_name) + " " + str(self.last_name)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "Telegram User"
        verbose_name_plural = "Telegram Users"
