from django.contrib import admin

from app.internal.users.db.models import TgUser


@admin.register(TgUser)
class UserAdmin(admin.ModelAdmin):
    pass
