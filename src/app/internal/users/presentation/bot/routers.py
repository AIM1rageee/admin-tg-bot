from telegram.ext import Application, CallbackQueryHandler, CommandHandler, ConversationHandler, MessageHandler, filters

from app.internal.common.checkers import HasPhoneNumberChecker, IsRegisteredChecker
from app.internal.users.domain.services import UserService
from app.internal.users.presentation.bot.handlers.change_favorites import ChangeFavoritesHandler, ChangeFavoritesState
from app.internal.users.presentation.bot.handlers.favorites import FavoritesHandler
from app.internal.users.presentation.bot.handlers.me import MeHandler
from app.internal.users.presentation.bot.handlers.set_phone import SetPhoneHandler, SetPhoneState
from app.internal.users.presentation.bot.handlers.start import StartHandler


def add_users_handlers(application: Application, user_service: UserService):
    is_registered_checker = IsRegisteredChecker(user_service)
    has_phone_number_checker = HasPhoneNumberChecker(user_service)

    start_handler = StartHandler(user_service)
    set_phone_handler = SetPhoneHandler(user_service, is_registered_checker)
    me_handler = MeHandler(user_service, is_registered_checker, has_phone_number_checker)
    favorites_handler = FavoritesHandler(user_service, is_registered_checker, has_phone_number_checker)
    change_favorites_handler = ChangeFavoritesHandler(user_service, is_registered_checker, has_phone_number_checker)

    application.add_handler(CommandHandler("start", start_handler.start_callback), group=0)
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("set_phone", set_phone_handler.set_phone_callback)],
            states={
                SetPhoneState.PHONE_EXTRACTION: [
                    MessageHandler(filters.TEXT & (~filters.COMMAND), set_phone_handler.phone_extraction_callback),
                ],
            },
            fallbacks=[MessageHandler(filters.COMMAND, set_phone_handler.cancel_callback)],
            per_user=True,
            per_chat=True,
            allow_reentry=True,
        ),
        group=1,
    )
    application.add_handler(CommandHandler("me", me_handler.me_callback), group=2)
    application.add_handler(CommandHandler("favorites", favorites_handler.favorites_callback), group=3)
    application.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("change_favorites", change_favorites_handler.change_favorites_callback)],
            states={
                ChangeFavoritesState.FAVORITE_CHANGE: [
                    CallbackQueryHandler(change_favorites_handler.add_favorite_callback, pattern="^add_favorite"),
                    CallbackQueryHandler(change_favorites_handler.remove_favorite_callback, pattern="^remove_favorite"),
                ],
                ChangeFavoritesState.FAVORITE_INPUT: [
                    MessageHandler(filters.TEXT & (~filters.COMMAND), change_favorites_handler.input_favorite_callback),
                ],
            },
            fallbacks=[MessageHandler(filters.COMMAND, change_favorites_handler.cancel_callback)],
            per_user=True,
            per_chat=True,
            allow_reentry=True,
        ),
        group=4,
    )
