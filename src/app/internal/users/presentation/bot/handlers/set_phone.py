from enum import IntEnum

from telegram import Update
from telegram.ext import CallbackContext, ConversationHandler

from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import IsRegisteredChecker
from app.internal.users.domain.services import UserService
from app.template_engine import render_template


class SetPhoneState(IntEnum):
    PHONE_EXTRACTION = 0


class SetPhoneHandler(IBotCommandHandler):
    def __init__(self, user_service: UserService, is_registered_checker: IsRegisteredChecker):
        super().__init__([is_registered_checker])
        self.user_service = user_service

    async def set_phone_callback(self, update: Update, context: CallbackContext) -> int:
        if not await self.check(update, context):
            return ConversationHandler.END
        await context.bot.send_message(update.message.chat_id, render_template("phone_input.html"), parse_mode="html")
        return SetPhoneState.PHONE_EXTRACTION

    async def phone_extraction_callback(self, update: Update, context: CallbackContext) -> int:
        if await self.user_service.try_update_user_phone(update.message.chat_id, update.message.text):
            await context.bot.send_message(
                update.message.chat_id,
                render_template("phone_saved.html"),
                parse_mode="html",
            )
            return ConversationHandler.END
        await context.bot.send_message(
            update.message.chat_id,
            render_template("wrong_phone_input.html"),
            parse_mode="html",
        )
        return SetPhoneState.PHONE_EXTRACTION
