from telegram import Update
from telegram.ext import CallbackContext

from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.users.domain.services import UserService
from app.template_engine import render_template


class StartHandler(IBotCommandHandler):
    def __init__(self, user_service: UserService):
        super().__init__([])
        self.user_service = user_service

    async def start_callback(self, update: Update, context: CallbackContext) -> None:
        user = await self.user_service.create_user(update.message.chat_id, update.message.from_user)
        await context.bot.send_message(
            chat_id=update.message.chat_id,
            text=render_template("start.html", user=user),
            parse_mode="html",
        )
