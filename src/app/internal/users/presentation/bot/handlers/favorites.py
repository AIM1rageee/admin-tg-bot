from telegram import Update
from telegram.ext import CallbackContext

from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import HasPhoneNumberChecker, IsRegisteredChecker
from app.internal.users.domain.services import UserService
from app.template_engine import render_template


class FavoritesHandler(IBotCommandHandler):
    def __init__(
        self,
        user_service: UserService,
        is_registered_checker: IsRegisteredChecker,
        has_phone_number_checker: HasPhoneNumberChecker,
    ):
        super().__init__([is_registered_checker, has_phone_number_checker])
        self.user_service = user_service

    async def favorites_callback(self, update: Update, context: CallbackContext):
        if not await self.check(update, context):
            return
        favorites = await self.user_service.get_favorites(update.message.chat_id)
        await context.bot.send_message(
            update.message.chat_id,
            render_template("favorites_info.html", favorites=favorites),
            parse_mode="html",
        )
