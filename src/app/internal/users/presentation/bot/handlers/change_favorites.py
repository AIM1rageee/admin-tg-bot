from enum import IntEnum

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import CallbackContext, ConversationHandler

from app.internal.common.bot_handler import IBotCommandHandler
from app.internal.common.checkers import HasPhoneNumberChecker, IsRegisteredChecker
from app.internal.users.domain.services import UserService


class ChangeFavoritesState(IntEnum):
    FAVORITE_CHANGE = 0
    FAVORITE_INPUT = 1


class ChangeFavoritesHandler(IBotCommandHandler):
    def __init__(
        self,
        user_service: UserService,
        is_registered_checker: IsRegisteredChecker,
        has_phone_number_checker: HasPhoneNumberChecker,
    ):
        super().__init__([is_registered_checker, has_phone_number_checker])
        self.user_service = user_service

    async def change_favorites_callback(self, update: Update, context: CallbackContext) -> int:
        if not await self.check(update, context):
            return ConversationHandler.END

        await update.message.reply_text(
            "Список ваших фаворитов:",
            reply_markup=await self.generate_favorites_list_markup(update.message.chat_id),
        )
        return ChangeFavoritesState.FAVORITE_CHANGE

    async def add_favorite_callback(self, update: Update, context: CallbackContext) -> int:
        query = update.callback_query
        await query.answer()

        await query.edit_message_text("➡️Введите имя вашего фаворита:")
        return ChangeFavoritesState.FAVORITE_INPUT

    async def input_favorite_callback(self, update: Update, context: CallbackContext) -> int:
        if await self.user_service.try_add_favorite(update.message.chat_id, update.message.text):
            await context.bot.send_message(update.message.chat_id, "✅Фаворит был успешно добавлен в ваш список!")
            return ConversationHandler.END
        await context.bot.send_message(update.message.chat_id, "❌Не удалось добавить фаворита. Попробуйте снова!")
        return ChangeFavoritesState.FAVORITE_INPUT

    async def remove_favorite_callback(self, update: Update, context: CallbackContext) -> int:
        query = update.callback_query
        await query.answer()

        user_id, favorite_username = query.data.split()[1:]
        user_id = int(user_id)
        if await self.user_service.try_remove_favorite(user_id, favorite_username):
            await query.edit_message_text("✅Фаворит был успешно удален из вашего списка!")
            return ConversationHandler.END
        await query.edit_message_text("❌Не удалось удалить фаворита. Попробуйте снова!")
        return ConversationHandler.END

    async def generate_favorites_list_markup(self, telegram_id: int):
        favorites = await self.user_service.get_favorites(telegram_id)
        keyboard = [
            [
                InlineKeyboardButton(
                    f"✖️{favorite}",
                    callback_data=f"remove_favorite {telegram_id} {favorite}",
                ),
            ]
            for favorite in favorites
        ]
        keyboard.append([InlineKeyboardButton("➕Добавить фаворита", callback_data="add_favorite")])
        reply_markup = InlineKeyboardMarkup(keyboard)
        return reply_markup
