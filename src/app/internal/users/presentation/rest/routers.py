from http import HTTPStatus

from ninja import NinjaAPI
from ninja.router import Router

from app.internal.common.response_entities import ErrorResponse, SuccessResponse
from app.internal.users.domain.entities import FavoriteUsersOut, TgUserIn, TgUserOut, TgUserPhoneIn
from app.internal.users.presentation.rest.handlers import UserHandlers


def get_users_router(user_handlers: UserHandlers) -> Router:
    router = Router(tags=["users"])

    @router.get(
        "/{telegram_id}",
        response={
            HTTPStatus.OK: TgUserOut,
            HTTPStatus.FORBIDDEN: ErrorResponse,
            HTTPStatus.NOT_FOUND: ErrorResponse,
        },
    )
    def get_user(request, telegram_id: int):
        return user_handlers.get_user(request, telegram_id)

    @router.put(
        "/{telegram_id}",
        response={
            HTTPStatus.OK: SuccessResponse,
            HTTPStatus.FORBIDDEN: ErrorResponse,
            HTTPStatus.NOT_FOUND: ErrorResponse,
        },
    )
    def update_user(request, telegram_id: int, payload: TgUserIn):
        return user_handlers.update_user(request, telegram_id, payload)

    @router.patch(
        "/{telegram_id}",
        response={
            HTTPStatus.OK: SuccessResponse,
            HTTPStatus.BAD_REQUEST: ErrorResponse,
            HTTPStatus.FORBIDDEN: ErrorResponse,
            HTTPStatus.NOT_FOUND: ErrorResponse,
        },
    )
    def update_user_phone(request, telegram_id: int, payload: TgUserPhoneIn):
        return user_handlers.update_user_phone(request, telegram_id, payload)

    @router.get(
        "/{telegram_id}/favorite_users",
        response={
            HTTPStatus.OK: FavoriteUsersOut,
            HTTPStatus.FORBIDDEN: ErrorResponse,
            HTTPStatus.NOT_FOUND: ErrorResponse,
        },
    )
    def get_favorite_users(request, telegram_id: int):
        return user_handlers.get_favorite_users(request, telegram_id)

    @router.post(
        "/{telegram_id}/favorite_users",
        response={HTTPStatus.CREATED: SuccessResponse, HTTPStatus.FORBIDDEN: ErrorResponse},
    )
    def add_favorite_user(request, telegram_id: int, username: str):
        return user_handlers.add_favorite_user(request, telegram_id, username)

    @router.delete(
        "/{telegram_id}/favorite_users/{username}",
        response={
            HTTPStatus.NO_CONTENT: SuccessResponse,
            HTTPStatus.FORBIDDEN: ErrorResponse,
            HTTPStatus.NOT_FOUND: ErrorResponse,
        },
    )
    def remove_favorite_user(request, telegram_id: int, username: str):
        return user_handlers.remove_favorite_user(request, telegram_id, username)

    return router


def add_users_router(api: NinjaAPI, user_handlers: UserHandlers):
    users_router = get_users_router(user_handlers)
    api.add_router("/users", users_router)
