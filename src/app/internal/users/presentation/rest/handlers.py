from http import HTTPStatus

from app.internal.common.response_entities import ErrorResponse, SuccessResponse
from app.internal.users.domain.entities import FavoriteUsersOut, TgUserIn, TgUserOut, TgUserPhoneIn
from app.internal.users.domain.services import UserService


class UserHandlers:
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def get_user(self, request, telegram_id: int):
        if request.user.telegram_id != telegram_id:
            return HTTPStatus.FORBIDDEN, ErrorResponse(message=HTTPStatus.FORBIDDEN.phrase)
        user = self.user_service.get_user_sync(telegram_id)
        if user is None:
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)
        return TgUserOut.from_orm(user)

    def update_user(self, request, telegram_id: int, data: TgUserIn):
        if request.user.telegram_id != telegram_id:
            return HTTPStatus.FORBIDDEN, ErrorResponse(message=HTTPStatus.FORBIDDEN.phrase)
        if not self.user_service.is_registered(telegram_id):
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)

        self.user_service.update_user(telegram_id, data)
        return SuccessResponse()

    def update_user_phone(self, request, telegram_id: int, data: TgUserPhoneIn):
        if request.user.telegram_id != telegram_id:
            return HTTPStatus.FORBIDDEN, ErrorResponse(message=HTTPStatus.FORBIDDEN.phrase)
        if not self.user_service.is_registered(telegram_id):
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)

        if not self.user_service.try_update_user_phone_sync(telegram_id, data.phone):
            return HTTPStatus.BAD_REQUEST, ErrorResponse(message=HTTPStatus.BAD_REQUEST.phrase)
        return SuccessResponse()

    def get_favorite_users(self, request, telegram_id: int):
        if request.user.telegram_id != telegram_id:
            return HTTPStatus.FORBIDDEN, ErrorResponse(message=HTTPStatus.FORBIDDEN.phrase)
        if not self.user_service.is_registered(telegram_id):
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)

        return FavoriteUsersOut(favorite_users=self.user_service.get_favorites_sync(telegram_id))

    def add_favorite_user(self, request, telegram_id: int, username: str):
        if request.user.telegram_id != telegram_id:
            return HTTPStatus.FORBIDDEN, ErrorResponse(message=HTTPStatus.FORBIDDEN.phrase)
        if not self.user_service.is_registered(telegram_id):
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)

        if not self.user_service.try_add_favorite_sync(telegram_id, username):
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.FORBIDDEN.phrase)

        return HTTPStatus.CREATED, SuccessResponse()

    def remove_favorite_user(self, request, telegram_id: int, username: str):
        if request.user.telegram_id != telegram_id:
            return HTTPStatus.FORBIDDEN, ErrorResponse(message=HTTPStatus.FORBIDDEN.phrase)
        if not self.user_service.is_registered(telegram_id):
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.NOT_FOUND.phrase)

        if not self.user_service.try_remove_favorite_sync(telegram_id, username):
            return HTTPStatus.NOT_FOUND, ErrorResponse(message=HTTPStatus.FORBIDDEN.phrase)

        return HTTPStatus.NO_CONTENT, SuccessResponse()
