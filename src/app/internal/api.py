from django.urls import path
from ninja import NinjaAPI

from app.internal.bank.domain.services import BankService
from app.internal.bank.presentation.rest.handlers import BankHandlers
from app.internal.bank.presentation.rest.routers import add_bank_router
from app.internal.tokens.domain.services import TokenService
from app.internal.tokens.presentation.rest.auth import HTTPJWTAuth
from app.internal.tokens.presentation.rest.handlers import TokenHandlers
from app.internal.tokens.presentation.rest.routers import add_tokens_router
from app.internal.users.domain.services import UserService
from app.internal.users.presentation.rest.handlers import UserHandlers
from app.internal.users.presentation.rest.routers import add_users_router


def build_api():
    api = NinjaAPI(title="DT.EDU.BACKEND", version="1.0.0", auth=[HTTPJWTAuth()])

    user_service = UserService()
    user_handlers = UserHandlers(user_service=user_service)
    add_users_router(api, user_handlers)

    token_service = TokenService()
    token_handlers = TokenHandlers(token_service=token_service)
    add_tokens_router(api, token_handlers)

    bank_service = BankService()
    bank_handlers = BankHandlers(bank_service=bank_service)
    add_bank_router(api, bank_handlers)

    return api


ninja_api = build_api()

urlpatterns = [
    path("", ninja_api.urls),
]
