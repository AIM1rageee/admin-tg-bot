from django.db import models

from app.internal.users.db.models import TgUser


class Token(models.Model):
    jti = models.CharField(max_length=255, primary_key=True)
    user = models.ForeignKey(TgUser, related_name="tokens", on_delete=models.CASCADE, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    revoked = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Token"
        verbose_name_plural = "Tokens"
