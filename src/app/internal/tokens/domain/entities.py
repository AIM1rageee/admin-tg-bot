from ninja import Schema


class TokenOut(Schema):
    access_token: str
    refresh_token: str
