from datetime import datetime, timedelta

import jwt

from app.internal.tokens.db.models import Token
from app.internal.tokens.domain.entities import TokenOut
from config.settings import JWT_SECRET


class TokenService:
    def check_token(self, jti, payload, token_type):
        try:
            token = Token.objects.get(pk=jti, revoked=False)
            iat = datetime.utcfromtimestamp(payload["iat"])
            exp = datetime.utcfromtimestamp(payload["exp"])
            now = datetime.utcnow()
            if payload["type"] == token_type and iat <= now < exp:
                return True
            token.revoked = True
            token.save(update_fields=("revoked",))
            return False
        except Exception:
            return False

    def generate_access_refresh_tokens(self, telegram_id) -> TokenOut:
        access_token = self.create_access_token(telegram_id)
        refresh_token = self.create_refresh_token(telegram_id)
        Token.objects.filter(user_id=telegram_id).update(revoked=True)
        Token.objects.bulk_create(
            [
                Token(jti=access_token, user_id=telegram_id),
                Token(jti=refresh_token, user_id=telegram_id),
            ],
        )
        return TokenOut(
            access_token=access_token,
            refresh_token=refresh_token,
        )

    def update_access_refresh_tokens(self, telegram_id, refresh_token) -> TokenOut:
        new_access_token = self.create_access_token(telegram_id)
        new_refresh_token = self.create_refresh_token(telegram_id)
        Token.objects.filter(pk=refresh_token).update(revoked=True)
        Token.objects.bulk_create(
            [
                Token(jti=new_access_token, user_id=telegram_id),
                Token(jti=new_refresh_token, user_id=telegram_id),
            ],
        )
        return TokenOut(
            access_token=new_access_token,
            refresh_token=new_refresh_token,
        )

    def create_access_token(self, telegram_id):
        jti = jwt.encode(
            {
                "type": "access",
                "telegram_id": telegram_id,
                "exp": datetime.utcnow() + timedelta(hours=3),
                "iat": datetime.utcnow(),
            },
            JWT_SECRET,
            algorithm="HS256",
        )
        return jti

    def create_refresh_token(self, telegram_id):
        jti = jwt.encode(
            {
                "type": "refresh",
                "telegram_id": telegram_id,
                "exp": datetime.utcnow() + timedelta(weeks=1),
                "iat": datetime.utcnow(),
            },
            JWT_SECRET,
            algorithm="HS256",
        )
        return jti
