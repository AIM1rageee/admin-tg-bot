from typing import Optional

import jwt
from django.http import HttpRequest
from ninja.security import HttpBearer

from app.internal.tokens.domain.services import TokenService
from app.internal.users.domain.services import UserService
from config.settings import JWT_SECRET


class HTTPJWTAuth(HttpBearer):
    token_service = TokenService()
    user_service = UserService()
    param_name = "access_token"

    def authenticate(self, request: HttpRequest, token: str) -> Optional[str]:
        try:
            payload = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
            if not self.token_service.check_token(token, payload, "access"):
                return None
            user = self.user_service.get_user_sync(payload["telegram_id"])
            if user is None:
                return None
            request.user = user
        except Exception:
            return None

        return token
