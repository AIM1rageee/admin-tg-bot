from http import HTTPStatus

import jwt

from app.internal.common.response_entities import ErrorResponse
from app.internal.tokens.domain.services import TokenService
from app.internal.users.db.models import TgUser
from config.settings import JWT_SECRET


class TokenHandlers:
    def __init__(self, token_service: TokenService):
        self.token_service = token_service

    def login(self, request, telegram_id: int):
        if not TgUser.objects.filter(pk=telegram_id).exists():
            return HTTPStatus.UNAUTHORIZED, ErrorResponse(message=HTTPStatus.UNAUTHORIZED.phrase)
        return self.token_service.generate_access_refresh_tokens(telegram_id)

    def update_tokens(self, request, refresh_token: str):
        try:
            refresh_payload = jwt.decode(refresh_token, JWT_SECRET, algorithms=["HS256"])
            if not self.token_service.check_token(refresh_token, refresh_payload, "refresh"):
                return HTTPStatus.UNAUTHORIZED, ErrorResponse(message=HTTPStatus.UNAUTHORIZED.phrase)
            return self.token_service.update_access_refresh_tokens(refresh_payload["telegram_id"], refresh_token)
        except Exception:
            return HTTPStatus.UNAUTHORIZED, ErrorResponse(message=HTTPStatus.UNAUTHORIZED.phrase)
