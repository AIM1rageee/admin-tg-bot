from http import HTTPStatus

from ninja import NinjaAPI
from ninja.router import Router

from app.internal.common.response_entities import ErrorResponse
from app.internal.tokens.domain.entities import TokenOut
from app.internal.tokens.presentation.rest.handlers import TokenHandlers


def get_tokens_router(token_handlers: TokenHandlers) -> Router:
    router = Router(tags=["auth"])

    @router.post("/login", response={HTTPStatus.OK: TokenOut, HTTPStatus.UNAUTHORIZED: ErrorResponse}, auth=None)
    def login(request, telegram_id: int):
        return token_handlers.login(request, telegram_id)

    @router.post(
        "/update_tokens",
        response={HTTPStatus.OK: TokenOut, HTTPStatus.UNAUTHORIZED: ErrorResponse},
        auth=None,
    )
    def update_tokens(request, refresh_token: str):
        return token_handlers.update_tokens(request, refresh_token)

    return router


def add_tokens_router(api: NinjaAPI, token_handlers: TokenHandlers):
    tokens_router = get_tokens_router(token_handlers)
    api.add_router("", tokens_router)
